package com.gannett.usat.fireflyapi.client;

public class FireflyAPIContext {

	protected static String endPointURL = null;
	protected static String endPointUserServiceURL = null;
	protected static String apiMarketId = "9999";
	public static boolean debugMode = false;
	public static String apiUserPwd = "";
	public static String apiUserID = "";
	public static String apiExternalSecret = null;
	public static String apiInternalSecret = null;

	
	public static String getEndPointURL() {
		return endPointURL;
	}

	public static void setEndPointURL(String endPointURL) {
		FireflyAPIContext.endPointURL = endPointURL;
	}

	public static String getApiMarketId() {
		return apiMarketId;
	}

	public static void setApiMarketId(String apiMarketId) {
		FireflyAPIContext.apiMarketId = apiMarketId;
	}

	public static boolean isDebugMode() {
		return debugMode;
	}

	public static void setDebugMode(boolean debugMode) {
		FireflyAPIContext.debugMode = debugMode;
	}

	public static String getEndPointUserServiceURL() {
		return endPointUserServiceURL;
	}

	public static void setEndPointUserServiceURL(String endPointUserServiceURL) {
		FireflyAPIContext.endPointUserServiceURL = endPointUserServiceURL;
	}

	public static String getApiUserPwd() {
		return apiUserPwd;
	}

	public static void setApiUserPwd(String apiUserPwd) {
		FireflyAPIContext.apiUserPwd = apiUserPwd;
	}

	public static String getApiUserID() {
		return apiUserID;
	}

	public static void setApiUserID(String apiUserID) {
		FireflyAPIContext.apiUserID = apiUserID;
	}

	public static String getApiExternalSecret() {
		return apiExternalSecret;
	}

	public static void setApiExternalSecret(String apiExternalSecret) {
		FireflyAPIContext.apiExternalSecret = apiExternalSecret;
	}

	public static String getApiInternalSecret() {
		return apiInternalSecret;
	}

	public static void setApiInternalSecret(String apiInternalSecret) {
		FireflyAPIContext.apiInternalSecret = apiInternalSecret;
	}
}