package com.gannett.usat.fireflyapi.client;

import java.lang.reflect.Type;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.message.BasicNameValuePair;

import com.gannett.usat.fireflyapi.domainbeans.ErrorsDeserializer;
import com.gannett.usat.fireflyapi.domainbeans.Meta;
import com.gannett.usat.fireflyapi.domainbeans.login.LoginResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

public class Login extends FireflyAPIBase {

	public static LoginResponse jsonToCreateUserResponse(String json) {
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.registerTypeAdapter(Meta.class, new ErrorsDeserializer());
		Gson gson = gsonBuilder.create();

		LoginResponse response = null;

		Type loginResponseType = new TypeToken<LoginResponse>() {
		}.getType();

		try {
			response = gson.fromJson(json, loginResponseType);
		} catch (Exception e) {
			e.printStackTrace();
			if (response == null) {
				response = new LoginResponse();
			}
			response.setRawResponse(json);
		}

		return response;
	}

	public LoginResponse getLogin(String emailAddress, String password) throws Exception {

		String responseJSON = null;

		URL url = new URL(this.getBaseAPIURL());

		URIBuilder builder = new URIBuilder();
		builder.setScheme(url.getProtocol()).setHost(url.getHost()).setPort(url.getPort()).setPath(url.getPath() + LOGIN_PATH);

		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

		if (emailAddress != null) {
			nameValuePairs.add(new BasicNameValuePair("username", emailAddress));
		}

		if (password != null) {
			nameValuePairs.add(new BasicNameValuePair("password", password));
		}

		if (FireflyAPIContext.getApiMarketId() != null) {
			nameValuePairs.add(new BasicNameValuePair("marketId", FireflyAPIContext.getApiMarketId()));
		}
		
		if (FireflyAPIContext.getApiExternalSecret() != null && !FireflyAPIContext.getApiExternalSecret().trim().equals("")) {
			nameValuePairs.add(new BasicNameValuePair("externalSecret", FireflyAPIContext.getApiExternalSecret()));			
		}

		// build up URL
		java.net.URI uri = builder.build();

		String uriString = uri.toString();

		responseJSON = this.makeAPIPostRequest(uriString, nameValuePairs);

		LoginResponse response = Login.jsonToCreateUserResponse(responseJSON);

		return response;

	}

}
