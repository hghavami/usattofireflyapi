package com.gannett.usat.fireflyapi.client;

import java.lang.reflect.Type;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.message.BasicNameValuePair;

import com.gannett.usat.fireflyapi.domainbeans.ErrorsDeserializer;
import com.gannett.usat.fireflyapi.domainbeans.Meta;
import com.gannett.usat.fireflyapi.domainbeans.autoLogin.AutoLoginResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

public class AutoLogin extends FireflyAPIBase {
	
	public static AutoLoginResponse jsonToCreateUserResponse(String json) {
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.registerTypeAdapter(Meta.class, new ErrorsDeserializer());
		Gson gson = gsonBuilder.create();

		AutoLoginResponse response = null;

		Type autoLoginResponseType = new TypeToken<AutoLoginResponse>() {
		}.getType();

		try {
			response = gson.fromJson(json, autoLoginResponseType);
		} catch (Exception e) {
			e.printStackTrace();
			if (response == null) {
				response = new AutoLoginResponse();
			}
			response.setRawResponse(json);
		}

		return response;
	}

	public AutoLoginResponse getAutoLogin(String autoLogin) throws Exception {

		String responseJSON = null;

		URL url = new URL(this.getBaseAPIURL());

		URIBuilder builder = new URIBuilder();
		builder.setScheme(url.getProtocol()).setHost(url.getHost()).setPort(url.getPort()).setPath(url.getPath() + AUTOLOGIN_PATH);

		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

		if (autoLogin != null) {
			nameValuePairs.add(new BasicNameValuePair("autologin", autoLogin));
		}

		// build up URL
		java.net.URI uri = builder.build();

		String uriString = uri.toString();

		responseJSON = this.makeAPIPostRequest(uriString, nameValuePairs);

		AutoLoginResponse response = AutoLogin.jsonToCreateUserResponse(responseJSON);

		return response;

	}


}
