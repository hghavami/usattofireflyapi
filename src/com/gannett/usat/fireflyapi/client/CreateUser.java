package com.gannett.usat.fireflyapi.client;

import java.lang.reflect.Type;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.message.BasicNameValuePair;

import com.gannett.usat.fireflyapi.domainbeans.ErrorsDeserializer;
import com.gannett.usat.fireflyapi.domainbeans.Meta;
import com.gannett.usat.fireflyapi.domainbeans.createUser.CreateUserResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

/**
 * This class is used to communicate with the Firefly API for vacation
 * stops/starts
 * 
 * @author hghavami
 * 
 */
public class CreateUser extends FireflyAPIBase {

	/**
	 * Converts json returned from the Firefly Subscription API to Objects
	 * 
	 * @param json
	 *            - A json string as returned from the API
	 * @return Collection of SubscriberAccount objects
	 */
	public static CreateUserResponse jsonToCreateUserResponse(String json) {
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.registerTypeAdapter(Meta.class, new ErrorsDeserializer());
		Gson gson = gsonBuilder.create();

		CreateUserResponse response = null;

		Type createUserResponseType = new TypeToken<CreateUserResponse>() {
		}.getType();

		try {
			response = gson.fromJson(json, createUserResponseType);
		} catch (Exception e) {
			e.printStackTrace();
			if (response == null) {
				response = new CreateUserResponse();
			}
			response.setRawResponse(json);
		}

		return response;
	}

	public CreateUserResponse createCreateUser(String firstName, String lastName, String emailAddress,
			String password) throws Exception {

		String responseJSON = null;

		URL url = new URL(this.getBaseAPIURL());

		URIBuilder builder = new URIBuilder();
		builder.setScheme(url.getProtocol()).setHost(url.getHost()).setPort(url.getPort()).setPath(url.getPath() + CREATE_USER_PATH);

		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

		if (firstName != null) {
			nameValuePairs.add(new BasicNameValuePair("firstName", firstName));			
		}
		
		if (lastName != null) {
			nameValuePairs.add(new BasicNameValuePair("lastName", lastName));			
		}
		
		if (emailAddress != null) {
			nameValuePairs.add(new BasicNameValuePair("email", emailAddress));
		}

		if (password != null) {
			nameValuePairs.add(new BasicNameValuePair("password", password));
		}

		if (FireflyAPIContext.getApiMarketId() != null) {
			nameValuePairs.add(new BasicNameValuePair("marketId", FireflyAPIContext.getApiMarketId()));
		}

		if (FireflyAPIContext.getApiExternalSecret() != null && !FireflyAPIContext.getApiExternalSecret().trim().equals("")) {
			nameValuePairs.add(new BasicNameValuePair("externalSecret", FireflyAPIContext.getApiExternalSecret()));
		}

		nameValuePairs.add(new BasicNameValuePair("authenticateUserInd", "true"));
		// build up URL
		java.net.URI uri = builder.build();

		String uriString = uri.toString();

		responseJSON = this.makeAPIPostRequest(uriString, nameValuePairs);

		CreateUserResponse response = CreateUser.jsonToCreateUserResponse(responseJSON);

		return response;

	}
}
