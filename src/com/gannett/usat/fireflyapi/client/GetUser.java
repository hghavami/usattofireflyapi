package com.gannett.usat.fireflyapi.client;

import java.lang.reflect.Type;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.message.BasicNameValuePair;

import com.gannett.usat.fireflyapi.domainbeans.ErrorsDeserializer;
import com.gannett.usat.fireflyapi.domainbeans.Meta;
import com.gannett.usat.fireflyapi.domainbeans.getUser.GetUserResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

/**
 * This class is used to communicate with the Firefly API for vacation
 * stops/starts
 * 
 * @author hghavami
 * 
 */
public class GetUser extends FireflyAPIBase {

	/**
	 * Converts json returned from the Firefly Subscription API to Objects
	 * 
	 * @param json
	 *            - A json string as returned from the API
	 * @return Collection of SubscriberAccount objects
	 */
	public static GetUserResponse jsonToCreateUserResponse(String json) {
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.registerTypeAdapter(Meta.class, new ErrorsDeserializer());
		Gson gson = gsonBuilder.create();

		GetUserResponse response = null;

		Type getUserResponseType = new TypeToken<GetUserResponse>() {
		}.getType();

		try {
			response = gson.fromJson(json, getUserResponseType);
		} catch (Exception e) {
			e.printStackTrace();
			if (response == null) {
				response = new GetUserResponse();
			}
			response.setRawResponse(json);
		}

		return response;
	}

	public GetUserResponse getGetUser(String emailAddress) throws Exception {

		String responseJSON = null;

		URL url = new URL(this.getBaseAPIURL());

		URIBuilder builder = new URIBuilder();
		builder.setScheme(url.getProtocol()).setHost(url.getHost()).setPort(url.getPort()).setPath(url.getPath() + GET_USER_PATH);

		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

		if (emailAddress != null) {
			nameValuePairs.add(new BasicNameValuePair("email", emailAddress));
		}
		
		if (FireflyAPIContext.getApiExternalSecret() != null && !FireflyAPIContext.getApiExternalSecret().trim().equals("")) {
			nameValuePairs.add(new BasicNameValuePair("externalSecret", FireflyAPIContext.getApiExternalSecret()));			
		}
		
		if (FireflyAPIContext.getApiInternalSecret() != null && !FireflyAPIContext.getApiInternalSecret().trim().equals("")) {
			nameValuePairs.add(new BasicNameValuePair("internalSecret", FireflyAPIContext.getApiInternalSecret()));
		}

		// build up URL
		java.net.URI uri = builder.build();

		String uriString = uri.toString();

		responseJSON = this.makeAPIPostRequest(uriString, nameValuePairs);

		GetUserResponse response = GetUser.jsonToCreateUserResponse(responseJSON);

		return response;

	}
}
