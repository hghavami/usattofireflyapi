package com.gannett.usat.fireflyapi.client;

import java.lang.reflect.Type;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.message.BasicNameValuePair;

import com.gannett.usat.fireflyapi.domainbeans.ErrorsDeserializer;
import com.gannett.usat.fireflyapi.domainbeans.Meta;
import com.gannett.usat.fireflyapi.domainbeans.updateUser.UpdateUserResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

/**
 * This class is used to communicate with the ICON API for vacation stops/starts
 * 
 * @author aeast
 * 
 */
public class UpdateUser extends FireflyAPIBase {

	/**
	 * Converts json returned from the ICON Subscription API to Objects
	 * 
	 * @param json
	 *            - A json string as returned from the API
	 * @return Collection of SubscriberAccount objects
	 */
	public static UpdateUserResponse jsonToUpdateUserResponse(String json) {
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.registerTypeAdapter(Meta.class, new ErrorsDeserializer());
		Gson gson = gsonBuilder.create();

		UpdateUserResponse response = null;

		Type updateEmailResponseType = new TypeToken<UpdateUserResponse>() {
		}.getType();

		try {
			response = gson.fromJson(json, updateEmailResponseType);
		} catch (Exception e) {
			e.printStackTrace();
			if (response == null) {
				response = new UpdateUserResponse();
			}
			response.setRawResponse(json);
		}

		return response;
	}

	public UpdateUserResponse createUpdateUser(String firstName, String lastName, String emailAddress, String userId) throws Exception {
	
		String responseJSON = null;
	
		URL url = new URL(this.getBaseAPIURL());
	
		URIBuilder builder = new URIBuilder();
		builder.setScheme(url.getProtocol()).setHost(url.getHost()).setPort(url.getPort()).setPath(url.getPath() + UPDATE_USER_PATH);
	
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
	
		if (userId != null) {
			nameValuePairs.add(new BasicNameValuePair("userId", userId));
		}
		
		if (firstName != null && !firstName.trim().equals("")) {
			nameValuePairs.add(new BasicNameValuePair("firstName", firstName));			
		}
		
		if (lastName != null && !lastName.trim().equals("")) {
			nameValuePairs.add(new BasicNameValuePair("lastName", lastName));			
		}
		
		if (emailAddress != null) {
			nameValuePairs.add(new BasicNameValuePair("email", emailAddress));
		}

		if (FireflyAPIContext.getApiMarketId() != null) {
			nameValuePairs.add(new BasicNameValuePair("marketId", FireflyAPIContext.getApiMarketId()));
		}
	
		if (FireflyAPIContext.getApiExternalSecret() != null && !FireflyAPIContext.getApiExternalSecret().trim().equals("")) {
			nameValuePairs.add(new BasicNameValuePair("externalSecret", FireflyAPIContext.getApiExternalSecret()));
		}

		if (FireflyAPIContext.getApiInternalSecret() != null && !FireflyAPIContext.getApiInternalSecret().trim().equals("")) {
			nameValuePairs.add(new BasicNameValuePair("internalSecret", FireflyAPIContext.getApiInternalSecret()));
		}

		// build up URL
		java.net.URI uri = builder.build();
	
		String uriString = uri.toString();
	
		responseJSON = this.makeAPIPostRequest(uriString, nameValuePairs);
	
		UpdateUserResponse response = UpdateUser.jsonToUpdateUserResponse(responseJSON);
	
		return response;
	
	}
}
