package com.gannett.usat.fireflyapi.unittest;

import com.gannett.usat.fireflyapi.client.FireflyAPIContext;
import com.gannett.usat.fireflyapi.domainbeans.ErrorsSerializer;
import com.gannett.usat.fireflyapi.domainbeans.Meta;
import com.gannett.usat.fireflyapi.domainbeans.changeUserPassword.ChangeUserPasswordResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class ChangeUserPasswordTest extends BaseTest {

	public static void main(String[] args) {

		ChangeUserPasswordTest test = new ChangeUserPasswordTest();

		test.setUpAPIContext();

		test.testChangeUserPassword();
	}

	protected void testChangeUserPassword() {

		try {
			com.gannett.usat.fireflyapi.client.ChangeUserPassword cUser = new com.gannett.usat.fireflyapi.client.ChangeUserPassword();

			FireflyAPIContext.setApiMarketId("USAT");
			FireflyAPIContext.setApiInternalSecret("HOLLY");

			ChangeUserPasswordResponse response = cUser.createChangeUserPassword("hghavami@usatoday.com", "password");

			String json = response.getRawResponse();

			System.out.println("Raw JSON: " + json);

			System.out.println("back to json....");

			GsonBuilder builder = new GsonBuilder();
			builder.setPrettyPrinting();
			builder.registerTypeAdapter(Meta.class, new ErrorsSerializer());

			Gson gson2 = builder.create();

			System.out.println(gson2.toJson(response));
			System.out.println(response.getErrorMessages().toString());
			System.out.println(response.getWarningCode().toString());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

