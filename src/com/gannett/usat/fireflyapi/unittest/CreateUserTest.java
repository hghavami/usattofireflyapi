package com.gannett.usat.fireflyapi.unittest;

import com.gannett.usat.fireflyapi.client.FireflyAPIContext;
import com.gannett.usat.fireflyapi.domainbeans.ErrorsSerializer;
import com.gannett.usat.fireflyapi.domainbeans.Meta;
import com.gannett.usat.fireflyapi.domainbeans.createUser.CreateUserResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class CreateUserTest extends BaseTest {

	public static void main(String[] args) {

		CreateUserTest test = new CreateUserTest();

		test.setUpAPIContext();

		test.testCreateCreateUser();
	}

	protected void testCreateCreateUser() {

		try {
			com.gannett.usat.fireflyapi.client.CreateUser cUser = new com.gannett.usat.fireflyapi.client.CreateUser();

			FireflyAPIContext.setApiMarketId("USAT");

			CreateUserResponse response = cUser.createCreateUser("Mark", "Jocobs", "hghavami20@gannett.com", "password");

			String json = response.getRawResponse();

			System.out.println("Raw JSON: " + json);

			System.out.println("back to json....");

			GsonBuilder builder = new GsonBuilder();
			builder.setPrettyPrinting();
			builder.registerTypeAdapter(Meta.class, new ErrorsSerializer());

			Gson gson2 = builder.create();

			System.out.println(gson2.toJson(response));
			System.out.println(response.getErrorMessages().toString());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

