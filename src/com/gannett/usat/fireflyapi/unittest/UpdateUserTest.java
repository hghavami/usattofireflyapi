package com.gannett.usat.fireflyapi.unittest;

import com.gannett.usat.fireflyapi.client.FireflyAPIContext;
import com.gannett.usat.fireflyapi.domainbeans.ErrorsSerializer;
import com.gannett.usat.fireflyapi.domainbeans.Meta;
import com.gannett.usat.fireflyapi.domainbeans.updateUser.UpdateUserResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class UpdateUserTest extends BaseTest {

	public static void main(String[] args) {

		UpdateUserTest test = new UpdateUserTest();

		test.setUpAPIContext();

		test.testCreateUpdateUser();
	}

	protected void testCreateUpdateUser() {

		try {
			com.gannett.usat.fireflyapi.client.UpdateUser uEmail = new com.gannett.usat.fireflyapi.client.UpdateUser();

			FireflyAPIContext.setApiMarketId("USAT");
			FireflyAPIContext.setApiInternalSecret("HOLLY");

			UpdateUserResponse response = uEmail.createUpdateUser("Houman", "Ghavami", "hghavami@usatoday.com", "143338001");

			String json = response.getRawResponse();

			System.out.println("Raw JSON: " + json);

			System.out.println("back to json....");

			GsonBuilder builder = new GsonBuilder();
			builder.setPrettyPrinting();
			builder.registerTypeAdapter(Meta.class, new ErrorsSerializer());

			Gson gson2 = builder.create();

			System.out.println(gson2.toJson(response));

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
