package com.gannett.usat.fireflyapi.unittest;

import com.gannett.usat.fireflyapi.client.FireflyAPIContext;
import com.gannett.usat.fireflyapi.domainbeans.ErrorsSerializer;
import com.gannett.usat.fireflyapi.domainbeans.Meta;
import com.gannett.usat.fireflyapi.domainbeans.login.LoginResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class LoginTest extends BaseTest {

	public static void main(String[] args) {

		LoginTest test = new LoginTest();

		test.setUpAPIContext();

		test.testGetLogin();
	}

	protected void testGetLogin() {

		try {
			com.gannett.usat.fireflyapi.client.Login cUser = new com.gannett.usat.fireflyapi.client.Login();

			FireflyAPIContext.setApiMarketId("USAT");
			FireflyAPIContext.setApiInternalSecret("HOLLY");

			LoginResponse response = cUser.getLogin("hghavami102@usatoday.com", "password");

			String json = response.getRawResponse();

			System.out.println("Raw JSON: " + json);

			System.out.println("back to json....");

			GsonBuilder builder = new GsonBuilder();
			builder.setPrettyPrinting();
			builder.registerTypeAdapter(Meta.class, new ErrorsSerializer());

			Gson gson2 = builder.create();

			System.out.println(gson2.toJson(response));
			System.out.println(response.getErrorMessages().toString());
			System.out.println(response.getUserId().toString());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

