package com.gannett.usat.fireflyapi.unittest;

import java.io.Console;
import java.io.InputStreamReader;

import com.gannett.usat.fireflyapi.client.FireflyAPIContext;

public class BaseTest {

	public String getInput(String prompt) {
		String input = null;

		if (prompt == null || prompt.length() == 0) {
			prompt = "Enter input [q to quit]:";
		}

		Console c = System.console();
		if (c == null) {

			InputStreamReader cin = new InputStreamReader(System.in);

			System.out.println(prompt);

			try {
				char[] cbuf = new char[256];
				cin.read(cbuf);

				String tempinput = new String(cbuf);
				if (tempinput != null && tempinput.trim().length() > 0) {
					input = tempinput.trim();
				}

			} catch (Exception e) {
				e.printStackTrace();
				System.exit(1);
			}
		} else {
			input = c.readLine(prompt);
		}

		if (input == null || input.trim().length() == 0) {
			;
		} else if (input.trim().equalsIgnoreCase("q")) {
			System.out.println("Exiting Normally.");
			System.exit(0);
		} else {
			input = input.trim();
		}

		return input;
	}

	public void setUpAPIContext() {
		FireflyAPIContext.setEndPointURL("http://10.187.140.23/UserService/Firefly.svc");
		FireflyAPIContext.setApiUserID("");
		FireflyAPIContext.setApiUserPwd("");
		FireflyAPIContext.setApiMarketId("USAT");
		FireflyAPIContext.setDebugMode(true);
	}
}
