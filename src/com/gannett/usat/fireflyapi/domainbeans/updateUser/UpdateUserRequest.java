package com.gannett.usat.fireflyapi.domainbeans.updateUser;

import java.io.Serializable;

public class UpdateUserRequest implements Serializable {

	private static final long serialVersionUID = -7129189501081935419L;
	private String message = null;
	private int status = 0;
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
}
