package com.gannett.usat.fireflyapi.domainbeans.autoLogin;

import java.io.Serializable;

public class AutoLoginRequestResponseUser implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = -723229415244892954L;
	private String userId = null;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
}
