package com.gannett.usat.fireflyapi.domainbeans.autoLogin;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import com.gannett.usat.fireflyapi.domainbeans.AtyponBaseAPIResponse;

public class AutoLoginResponse extends AtyponBaseAPIResponse implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1644857383343352479L;
	private AutoLoginRequestMeta meta = null;
	private AutoLoginRequestResponse response = null;
	

	@Override
	public boolean containsErrors() {
		boolean containsErrors = false;
		if (this.meta != null) {
			// User profile available or pending password reset, allow customer access 
			if ((!this.meta.getMessage().trim().equals("Success") || this.meta.getStatus() > 0) && this.meta.getStatus() != 303) {
				containsErrors = true;
			}
		} else {
			if (this.getRawResponse() != null && this.getRawResponse().length() > 0) {
				containsErrors = true;
			}
		}
		return containsErrors;
	}

	@Override
	public Collection<String> getErrorMessages() {
		Collection<String> messages = new ArrayList<String>();
		String aMessage = "";
		if (this.meta != null) {
			if (meta.getMessage().trim().equals("Success") || meta.getStatus() == 0) {
				aMessage = "";  //Success, no message is returned
			} else {
				aMessage = meta.getMessage();				
			}
			messages.add(aMessage);
		} else {
			if (this.getRawResponse() != null && this.getRawResponse().length() > 0) {
				messages.add(this.getRawResponse());
			}
		}
		return messages;
	}

	public String getUserId() {
		if (this.response == null || this.response.getUser() == null || this.response.getUser().getUserId() == null) {
			return "";
		} else {
			return this.response.getUser().getUserId();			
		}
	}

	public AutoLoginRequestMeta getMeta() {
		return meta;
	}

	public void setMeta(AutoLoginRequestMeta meta) {
		this.meta = meta;
	}

	public AutoLoginRequestResponse getResponse() {
		return response;
	}

	public void setResponse(AutoLoginRequestResponse response) {
		this.response = response;
	}
}