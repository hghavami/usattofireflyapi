package com.gannett.usat.fireflyapi.domainbeans.autoLogin;

import java.io.Serializable;

public class AutoLoginRequestMeta implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 6484048167639385112L;
	/**
	 * 
	 */
	private String message = null;
	private int status = 0;
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
}
