package com.gannett.usat.fireflyapi.domainbeans.autoLogin;

import java.io.Serializable;

public class AutoLoginRequestResponse implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 3002615955904252458L;
	/**
	 * 
	 */

	private String autologin = null;
	private String sessionKey = null;
	
	private AutoLoginRequestResponseUser user = null;
	public String getAutologin() {
		return autologin;
	}
	public void setAutologin(String autologin) {
		this.autologin = autologin;
	}
	public String getSessionKey() {
		return sessionKey;
	}
	public void setSessionKey(String sessionKey) {
		this.sessionKey = sessionKey;
	}
	public AutoLoginRequestResponseUser getUser() {
		return user;
	}
	public void setUser(AutoLoginRequestResponseUser user) {
		this.user = user;
	}
	
	
}
