package com.gannett.usat.fireflyapi.domainbeans;

import java.io.Serializable;
import java.util.Collection;

public abstract class AtyponBaseAPIResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1604147227842316449L;

	private transient String rawResponse = null;

	public String getRawResponse() {
		return rawResponse;
	}

	public void setRawResponse(String rawResponse) {
		this.rawResponse = rawResponse;
	}

	public abstract boolean containsErrors();

	public abstract Collection<String> getErrorMessages();

	public boolean redirectResponse() {
		if (this.rawResponse != null && this.rawResponse.startsWith("<!DOCTYPE html")) {
			return true;
		}
		return false;
	}
}
