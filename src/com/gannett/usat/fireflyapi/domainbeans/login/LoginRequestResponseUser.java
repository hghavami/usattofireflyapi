package com.gannett.usat.fireflyapi.domainbeans.login;

import java.io.Serializable;

public class LoginRequestResponseUser implements Serializable {


	private static final long serialVersionUID = -3048415086249552848L;
	private String userId = null;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
}
