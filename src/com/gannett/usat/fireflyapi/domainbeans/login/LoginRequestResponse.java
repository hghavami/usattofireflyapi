package com.gannett.usat.fireflyapi.domainbeans.login;

import java.io.Serializable;

public class LoginRequestResponse implements Serializable {

	private static final long serialVersionUID = 7707211667728137662L;
	private String autologin = null;
	private String sessionKey = null;
	
	public String getSessionKey() {
		return sessionKey;
	}

	public void setSessionKey(String sessionKey) {
		this.sessionKey = sessionKey;
	}

	public String getAutologin() {
		return autologin;
	}

	public void setAutologin(String autologin) {
		this.autologin = autologin;
	}

	private LoginRequestResponseUser user = null;

	public LoginRequestResponseUser getUser() {
		return user;
	}

	public void setUserId(LoginRequestResponseUser user) {
		this.user = user;
	}
	
}
