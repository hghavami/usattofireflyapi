package com.gannett.usat.fireflyapi.domainbeans.login;

import java.io.Serializable;

public class LoginRequestMeta implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 5666328334856604545L;
	private String message = null;
	private int status = 0;
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
}
