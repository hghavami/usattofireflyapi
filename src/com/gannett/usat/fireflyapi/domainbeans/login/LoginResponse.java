package com.gannett.usat.fireflyapi.domainbeans.login;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import com.gannett.usat.fireflyapi.domainbeans.AtyponBaseAPIResponse;

public class LoginResponse extends AtyponBaseAPIResponse implements Serializable {

	private static final long serialVersionUID = -2170921093987929790L;
	private LoginRequestMeta meta = null;
	private LoginRequestResponse response = null;
	
	public LoginRequestMeta getMeta() {
		return meta;
	}

	public void setMeta(LoginRequestMeta meta) {
		this.meta = meta;
	}

	public LoginRequestResponse getResponse() {
		return response;
	}

	public void setResponse(LoginRequestResponse response) {
		this.response = response;
	}


	@Override
	public boolean containsErrors() {
		boolean containsErrors = false;
		if (this.meta != null) {
			// User profile available or pending password reset, allow customer access 
			if ((!this.meta.getMessage().trim().equals("Success") || this.meta.getStatus() > 0) && this.meta.getStatus() != 303) {
				containsErrors = true;
			}
		} else {
			if (this.getRawResponse() != null && this.getRawResponse().length() > 0) {
				containsErrors = true;
			}
		}
		return containsErrors;
	}

	@Override
	public Collection<String> getErrorMessages() {
		Collection<String> messages = new ArrayList<String>();
		String aMessage = "";
		if (this.meta != null) {
			if (meta.getMessage().trim().equals("Success") || meta.getStatus() == 0) {
				aMessage = "";  //Success, no message is returned
			} else {
				aMessage = meta.getMessage();				
			}
			messages.add(aMessage);
		} else {
			if (this.getRawResponse() != null && this.getRawResponse().length() > 0) {
				messages.add(this.getRawResponse());
			}
		}
		return messages;
	}

	public String getUserId() {
		if (this.response == null || this.response.getUser() == null || this.response.getUser().getUserId() == null) {
			return "";
		} else {
			return this.response.getUser().getUserId();			
		}
	}
}