package com.gannett.usat.fireflyapi.domainbeans.changeUserPassword;

import java.io.Serializable;

public class ChangeUserPasswordRequestResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8864176051231547732L;
	private String warningCode = null;
	private String warningMessage = null;
	
	public String getWarningCode() {
		return warningCode;
	}
	public void setWarningCode(String warningcode) {
		this.warningCode = warningcode;
	}
	public String getWarningMessage() {
		return warningMessage;
	}
	public void setWarningMessage(String warningMessage) {
		this.warningMessage = warningMessage;
	}
	
}
