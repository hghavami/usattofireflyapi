package com.gannett.usat.fireflyapi.domainbeans.changeUserPassword;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import com.gannett.usat.fireflyapi.domainbeans.AtyponBaseAPIResponse;

public class ChangeUserPasswordResponse extends AtyponBaseAPIResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 384927228515265120L;
	private ChangeUserPasswordRequestMeta meta = null;
	private ChangeUserPasswordRequestResponse response = null;

	public ChangeUserPasswordRequestMeta getMeta() {
		return meta;
	}

	public void setMeta(ChangeUserPasswordRequestMeta meta) {
		this.meta = meta;
	}

	public ChangeUserPasswordRequestResponse getResponse() {
		return response;
	}

	public void setResponse(ChangeUserPasswordRequestResponse response) {
		this.response = response;
	}

	@Override
	public boolean containsErrors() {
		boolean containsErrors = false;
		if (this.meta != null) {
			if (!this.meta.getMessage().trim().equals("Success") || this.meta.getStatus() > 0) {
				containsErrors = true;
			}
		} else {
			if (this.getRawResponse() != null && this.getRawResponse().length() > 0) {
				containsErrors = true;
			}
		}
		return containsErrors;
	}

	@Override
	public Collection<String> getErrorMessages() {
		Collection<String> messages = new ArrayList<String>();
		String aMessage = "";
		if (this.meta != null) {
			if (meta.getMessage().trim().equals("Success") || meta.getStatus() == 0) {
				aMessage = "";  //Success, no message is returned
			} else {
				aMessage = meta.getMessage();				
			}
			messages.add(aMessage);
		} else {
			if (this.getRawResponse() != null && this.getRawResponse().length() > 0) {
				messages.add(this.getRawResponse());
			}
		}
		return messages;
	}

	public String getWarningCode() {
		if (response == null || response.getWarningCode() == null) {
			return "";
		} else {
			return response.getWarningCode();			
		}
	}
}