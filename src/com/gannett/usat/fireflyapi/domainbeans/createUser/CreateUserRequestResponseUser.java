package com.gannett.usat.fireflyapi.domainbeans.createUser;

import java.io.Serializable;

public class CreateUserRequestResponseUser implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4964642926545625135L;
	private String userId = null;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
}
