package com.gannett.usat.fireflyapi.domainbeans.createUser;

import java.io.Serializable;

public class CreateUserRequestMeta implements Serializable {

	private static final long serialVersionUID = -6721916830391008240L;

	private String message = null;
	private int status = 0;
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
}
