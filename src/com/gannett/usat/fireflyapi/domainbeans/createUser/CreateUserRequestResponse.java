package com.gannett.usat.fireflyapi.domainbeans.createUser;

import java.io.Serializable;

public class CreateUserRequestResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1230825324546568032L;
	private CreateUserRequestResponseUser user = null;

	public CreateUserRequestResponseUser getUser() {
		return user;
	}

	public void setUserId(CreateUserRequestResponseUser user) {
		this.user = user;
	}
}
