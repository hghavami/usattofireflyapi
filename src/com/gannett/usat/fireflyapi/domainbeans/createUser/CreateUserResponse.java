package com.gannett.usat.fireflyapi.domainbeans.createUser;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import com.gannett.usat.fireflyapi.domainbeans.AtyponBaseAPIResponse;

public class CreateUserResponse extends AtyponBaseAPIResponse implements Serializable {

	private static final long serialVersionUID = 8286950916983169524L;

	private CreateUserRequestMeta meta = null;
	private CreateUserRequestResponse response = null;


	public CreateUserRequestMeta getMeta() {
		return meta;
	}

	public void setMeta(CreateUserRequestMeta meta) {
		this.meta = meta;
	}

	public CreateUserRequestResponse getResponse() {
		return response;
	}

	public void setResponse(CreateUserRequestResponse response) {
		this.response = response;
	}

	@Override
	public boolean containsErrors() {
		boolean containsErrors = false;
		if (this.meta != null) {
			if (!this.meta.getMessage().trim().equals("Success") || this.meta.getStatus() > 0) {
				containsErrors = true;
			}
		} else {
			if (this.getRawResponse() != null && this.getRawResponse().length() > 0) {
				containsErrors = true;
			}
		}
		return containsErrors;
	}

	@Override
	public Collection<String> getErrorMessages() {
		Collection<String> messages = new ArrayList<String>();
		String aMessage = "";
		if (this.meta != null) {
			if (meta.getMessage().trim().equals("Success") || meta.getStatus() == 0) {
				aMessage = "";  //Success, no message is returned
			} else {
				aMessage = meta.getMessage();				
			}
			messages.add(aMessage);
		} else {
			if (this.getRawResponse() != null && this.getRawResponse().length() > 0) {
				messages.add(this.getRawResponse());
			}
		}
		return messages;
	}
	
	public String getUserId() {
		if (this.response == null || this.response.getUser().getUserId() == null) {
			return "";
		} else {
			return this.response.getUser().getUserId();			
		}
	}

}
