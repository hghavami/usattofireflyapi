package com.gannett.usat.fireflyapi.domainbeans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

public class Meta implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7106683966594682632L;

	private Collection<Error> meta = null;

	public Meta() {
		super();
		this.meta = new ArrayList<Error>();
	}

	public Collection<Error> getErrors() {
		return meta;
	}

	public void setErrors(Collection<Error> errors) {
		this.meta = errors;
	}

	public void addError(Error err) {
		this.meta.add(err);
	}

}
