package com.gannett.usat.fireflyapi.domainbeans.changePassword;

import java.io.Serializable;

public class ChangePasswordRequestResponse implements Serializable {

	private static final long serialVersionUID = -741456155936064139L;

	private String warningCode = null;
	private String warningMessage = null;
	
	public String getWarningCode() {
		return warningCode;
	}
	public void setWarningCode(String warningcode) {
		this.warningCode = warningcode;
	}
	public String getWarningMessage() {
		return warningMessage;
	}
	public void setWarningMessage(String warningMessage) {
		this.warningMessage = warningMessage;
	}
	
}
