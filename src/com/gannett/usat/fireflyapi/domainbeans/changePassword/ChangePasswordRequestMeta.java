package com.gannett.usat.fireflyapi.domainbeans.changePassword;

import java.io.Serializable;

public class ChangePasswordRequestMeta implements Serializable {

	private static final long serialVersionUID = 8424071169177171191L;

	private String message = null;
	private int status = 0;
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
}
