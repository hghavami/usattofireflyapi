package com.gannett.usat.fireflyapi.domainbeans.resetPassword;

import java.io.Serializable;

public class ResetPasswordRequestResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3273869422086336488L;
	private String warningCode = null;
	private String warningMessage = null;
	
	public String getWarningCode() {
		return warningCode;
	}
	public void setWarningCode(String warningcode) {
		this.warningCode = warningcode;
	}
	public String getWarningMessage() {
		return warningMessage;
	}
	public void setWarningMessage(String warningMessage) {
		this.warningMessage = warningMessage;
	}
	
}
