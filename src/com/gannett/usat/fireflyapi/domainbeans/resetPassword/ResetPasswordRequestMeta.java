package com.gannett.usat.fireflyapi.domainbeans.resetPassword;

import java.io.Serializable;

public class ResetPasswordRequestMeta implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7375040798341416826L;
	private String message = null;
	private int status = 0;
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
}
