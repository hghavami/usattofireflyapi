package com.gannett.usat.fireflyapi.domainbeans.getUser;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import com.gannett.usat.fireflyapi.domainbeans.AtyponBaseAPIResponse;

public class GetUserResponse extends AtyponBaseAPIResponse implements Serializable {

	private static final long serialVersionUID = -3140145859111085771L;
	private GetUserRequestMeta meta = null;
	private GetUserRequestResponse response = null;

	public GetUserRequestMeta getMeta() {
		return meta;
	}

	public void setMeta(GetUserRequestMeta meta) {
		this.meta = meta;
	}

	public GetUserRequestResponse getResponse() {
		return response;
	}

	public void setResponse(GetUserRequestResponse response) {
		this.response = response;
	}

	@Override
	public boolean containsErrors() {
		boolean containsErrors = false;
		if (this.meta != null) {
			if (!this.meta.getMessage().trim().equals("Success") || this.meta.getStatus() > 0) {
				containsErrors = true;
			}
		} else {
			if (this.getRawResponse() != null && this.getRawResponse().length() > 0) {
				containsErrors = true;
			}
		}
		return containsErrors;
	}

	@Override
	public Collection<String> getErrorMessages() {
		Collection<String> messages = new ArrayList<String>();
		String aMessage = "";
		if (this.meta != null) {
			if (meta.getMessage().trim().equals("Success") || meta.getStatus() == 0) {
				aMessage = "";  //Success, no message is returned
			} else {
				aMessage = meta.getMessage();				
			}
			messages.add(aMessage);
		} else {
			if (this.getRawResponse() != null && this.getRawResponse().length() > 0) {
				messages.add(this.getRawResponse());
			}
		}
		return messages;
	}

	public String getUserId() {
		if (this.response == null || this.response.getUserId() == null) {
			return "";
		} else {
			return this.response.getUserId();			
		}
	}
}