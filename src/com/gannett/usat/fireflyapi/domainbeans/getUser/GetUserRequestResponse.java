package com.gannett.usat.fireflyapi.domainbeans.getUser;

import java.io.Serializable;

public class GetUserRequestResponse implements Serializable {

	private static final long serialVersionUID = 1273260117953237274L;

	private String userId = null;
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
}
